package com.ar9013.ar;

import android.content.Context;
import android.os.Build;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.hardware.Camera;

import java.io.IOException;

/**
 * Created by luokangyu on 2017/8/8.
 */

public class CameraSurface extends SurfaceView implements SurfaceHolder.Callback{

    private Camera camera = null;

    public CameraSurface(Context context) {
        super(context);

        getHolder().addCallback(this);

        if(Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB)
            getHolder().setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
    }

    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        camera = Camera.open(0);
        camera.setDisplayOrientation(90);
    }


    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        // This method is called when the surface changes, e.g. when it's size
        // is set.
        // We use the opportunity to initialize the camera preview display
        // dimensions.
        Camera.Parameters p = camera.getParameters();
//		p.setPreviewSize(width, height);//old 会报 setParameters failed
        camera.setParameters(p);

        // We also assign the preview display to this surface...
        try {
            camera.setPreviewDisplay(holder);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        // Once the surface gets destroyed, we stop the preview mode and release
        // the whole camera since we no longer need it.
        camera.stopPreview();
        camera.release();
        camera = null;
    }

    public Camera getCamera(){return camera;}
}
